An implementation of the Redactor (http://imperavi.com/redactor/) text editor for Django.

* Requires the jQuery library (works with 1.7.2, not tested on others)

Make sure you buy your own license here: http://imperavi.com/redactor/

For images and files upload and management, please use cotidia-filemananger app

*** IE7 Notice ***

Since IE7 does not recognise the "application/json" mimetype, you will have to amend the filemanager views to return the json response of the uploaded file to the following mimetype: "text/html"

If your website is not supported on IE7, then just don't worry about it.
