import os
from distutils.core import setup
from setuptools import find_packages


VERSION = __import__("redactor").VERSION

CLASSIFIERS = [
    'Framework :: Django',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Topic :: Software Development',
]

install_requires = [
    #Packages
]

# taken from django-registration
# Compile the list of packages available, because distutils doesn't have
# an easy way to do this.
packages, data_files = [], []
root_dir = os.path.dirname(__file__)
if root_dir:
    os.chdir(root_dir)

for dirpath, dirnames, filenames in os.walk('redactor'):
    # Ignore dirnames that start with '.'
    for i, dirname in enumerate(dirnames):
        if dirname.startswith('.'): del dirnames[i]
    if '__init__.py' in filenames:
        pkg = dirpath.replace(os.path.sep, '.')
        if os.path.altsep:
            pkg = pkg.replace(os.path.altsep, '.')
        packages.append(pkg)
    elif filenames:
        prefix = dirpath[9:] # Strip "redactor/" or "redactor\"
        for f in filenames:
            data_files.append(os.path.join(prefix, f))


setup(
    name="cotidia-redactor",
    description="Text editor integrated with Django",
    version=VERSION,
    author="Guillaume Piot",
    author_email="guillaume@cotidia.com",
    url="https://bitbucket.org/guillaumepiot/cotidia-redactor",
    download_url="https://bitbucket.org/guillaumepiot/cotidia-redactor/downloads/cotidia-redactor-%s.tar.gz" % VERSION,
    package_dir={'redactor': 'redactor'},
    packages=packages,
    package_data={'redactor': data_files},
    include_package_data=True,
    install_requires=install_requires,
    classifiers=CLASSIFIERS,
)
